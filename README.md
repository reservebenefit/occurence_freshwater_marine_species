# Mapping fish species diversity


 * Marine fish species data were obtained from the Ocean Biogeographic Information System [OBIS](http://www.iobis.org). 
 * We inventoried 16,238,200 occurrence records from 34,883 entries. 
 * We cleaned the data by identifying synonyms, misspellings and rare species (only one occurrence) and restricted them to species present in the marine environment according to FishBase.

# Data

## Inputs

* [data](data): shapefiles of basins
* [data/Shp_species](data/Shp_species): shapefiles of species geographical distribution
* [grid_equalarea200km](grid_equalarea200km): shapefile of a 200km² equalarea nested grid worlmap
* [equalarea_id_coords.tsv](equalarea_id_coords.tsv): table of ID/coordinates of each geographic cells of the grid

## Results

* [equalarea_id_coordsCA_FWRS_MR_RS.csv](equalarea_id_coordsCA_FWRS_MR_RS.csv): table of ID/coordinates/occurences of freshwater and marine species of each geographic cells of the grid
* [rdata/Mat_PA_Fw_Fish.rdata](rdata/Mat_PA_Fw_Fish.rdata):  1° grid resolution with occurences of freshwater species
* [rdata/Mat_PA_GaspObis.Rdata](rdata/Mat_PA_GaspObis.Rdata): species occurence table 

# Source code

* [fw_fis_pa_emh.R](fw_fis_pa_emh.R): from shapefiles of species, it generates table of occurence in a 1° resolution grid covering all oceans
* [occurence_FW_MR_grid_equalarea200km.R](occurence_FW_MR_grid_equalarea200km.R): from species occurence table, it generates table of ID/coordinates of each geographic cells of the grid






# Methods 



Marine fish species data were obtained from the Ocean Biogeographic Information System [OBIS](http://www.iobis.org). We inventoried 16,238,200 occurrence records from 34,883 entries. We cleaned the data by identifying synonyms, misspellings and rare species (only one occurrence) and restricted them to species present in the marine environment according to FishBase. 

We reconstructed distribution maps for each species, defined as the convex polygon surrounding the area where each species was observed. The resulting polygon was divided into four parts across the world to integrate possible discontinuity between the two hemispheres and the Atlantic and Pacific Oceans. 

For example, antitropical species are distributed in northern and southern hemisphere, but show range discontinuity near the tropics and a polygon division can account for this singularity. We refined each species distribution map by removing areas where maximum depths fell outside the minimum or maximum known depth range of the species. 

As the OBIS database does not properly represent tropical fish assemblages, we merged this database with the Gaspar database at 1° resolution that encompass 6316 coral reef species. 

We obtained a world database containing most marine fish species that we aggregated on a 1° resolution grid covering all oceans, as this resolution is useful for other projects. A freshwater dataset was obtained from the occurrence data provided by Tedesco et al. We first compiled the polygon of each available species from the occurrence table, and then aggregated this information into a presence-absence matrix. We obtained a world database containing freshwater fish species on a 1° resolution grid covering all terrestrial parts of the Earth. Considering the scale and resolution of the current study (200 km), our polygons represent the distribution of species with sufficient accuracy compared to those commonly used in macroecological studies.


# Authors

**Camille ALBOUY, IFREMER**
Pierre-Edouard GUERIN, EPHE, CNRS
Stephanie MANEL, EPHE, CNRS 
Loic PELLISSIER, ETH Zurich


# License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


